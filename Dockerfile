FROM tomcat:8.0
MAINTAINER hallcr
COPY target/*.war /usr.local/tomcat/webapps/hello-dino.war
expose 8080
CMD ["catalina.sh", "run"]
